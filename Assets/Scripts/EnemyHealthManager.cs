﻿using UnityEngine;
using System.Collections;

public class EnemyHealthManager : MonoBehaviour 
{
	public int enemyHealth;

	public GameObject deathParticle;

	public int pointsOnDeath;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (enemyHealth <= 0) 
		{
			Instantiate (deathParticle, transform.position, transform.rotation);
			ScoreManager.AddPoints (pointsOnDeath);
			Destroy (gameObject);
		}
	}

	public void giveDamage (int damageToGive)
	{
		enemyHealth -= damageToGive;
		GetComponent<AudioSource> ().Play ();
	}
}
