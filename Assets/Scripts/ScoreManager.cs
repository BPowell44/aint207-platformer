﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour 
{
	// Store's the score
	public static int score;

	// Reference to the text
	Text text;

	void Start()
	{
		// Setting up the references
		text = GetComponent<Text> ();
		//score = 0;

		score = PlayerPrefs.GetInt ("PlayerCurrentScore");

	}

	void Update()
	{
		// The minumium show the player can have is "0"
		if (score < 0)
			score = 0;

		// Shows text and adds in the score
		text.text = "" + score;
	}

	public static void AddPoints (int pointsToAdd)
	{
		// Add's "50" score points on each coin pickup
		score += pointsToAdd;
		PlayerPrefs.SetInt ("PlayerCurrentScore", score);
	}

	public static void Reset()
	{
		// Used to reset score after death
		score = 0;
		PlayerPrefs.SetInt ("PlayerCurrentScore", score);
	}
}