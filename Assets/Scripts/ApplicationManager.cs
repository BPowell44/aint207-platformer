﻿using UnityEngine;
using System.Collections;

public class ApplicationManager : MonoBehaviour 
{
	public int lives;

	public void StartGame ()
	{
		PlayerPrefs.SetInt ("PlayerCurrentLives", lives);
		PlayerPrefs.SetInt ("PlayerCurrentScore", 0);
		PlayerPrefs.SetInt ("PlayerHighScore", 0);
		Application.LoadLevel("TutorialLevel");
	}

	public void Quit () 
	{
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit();
		#endif
	}
}
