using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{

	public float moveSpeed;
	private float moveVelocity;
	public float jumpHeight;

	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask whatIsGround;
	private bool grounded;

	private bool doubleJumped;

	private Animator anim;

	public Transform firePoint;
	public GameObject fireBall;

	public float shotDelay;
	private float shotDelayCounter;

	public float knockBack;
	public float knockBackLength;
	public float knockbackCounter;
	public bool knockFromRight;

	public AudioSource jumpingSound;
	public AudioSource walkingSound;

	void Start () 
	{
		anim = GetComponent<Animator> ();
	}

	void FixedUpdate()
	{
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);
	}

	void Update ()
	{
		if (grounded)
			doubleJumped = false;

		anim.SetBool ("Grounded", grounded);


		if (Input.GetKeyDown (KeyCode.Space) && grounded)
		{
			Jump ();
			jumpingSound.Play ();
		}

		if (Input.GetKeyDown (KeyCode.Space) && !doubleJumped && !grounded)
		{
			Jump ();
			doubleJumped = true;
			jumpingSound.Play ();
		}

		moveVelocity = 0f;

		if(Input.GetKey (KeyCode.D))
		{
			moveVelocity = moveSpeed;
		}

		if(Input.GetKey (KeyCode.A))
		{
			moveVelocity = -moveSpeed;
		}

		if(Input.GetKey (KeyCode.D) && grounded == true)
		{
			if (walkingSound.isPlaying != true)
			{
				walkingSound.Play ();
			}
		}
		
		if(Input.GetKey (KeyCode.A) && grounded == true)
		{
			if (walkingSound.isPlaying != true)
			{
				walkingSound.Play ();
			}
		}
		if (knockbackCounter <= 0) 
		{
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (moveVelocity, GetComponent<Rigidbody2D> ().velocity.y);
		
		} else {
						
			if (knockFromRight)
				GetComponent<Rigidbody2D> ().velocity = new Vector2 (-knockBack, knockBack);
			if (!knockFromRight)
				GetComponent<Rigidbody2D> ().velocity = new Vector2 (knockBack, knockBack);

				knockbackCounter -= Time.deltaTime;
		}

		anim.SetFloat ("Speed", Mathf.Abs(GetComponent<Rigidbody2D> ().velocity.x));

		if (GetComponent<Rigidbody2D> ().velocity.x > 0)
			transform.localScale = new Vector3 (1f, 1f, 1f);
		else if (GetComponent<Rigidbody2D> ().velocity.x < 0)
			transform.localScale = new Vector3 (-1f, 1f, 1f);

		if (Input.GetKeyDown (KeyCode.F)) 
		{
			Instantiate (fireBall, firePoint.position, firePoint.rotation);
			shotDelayCounter = shotDelay;
		}

		if (Input.GetKeyDown (KeyCode.F))
		{
			shotDelayCounter = shotDelay;
			shotDelayCounter -= Time.deltaTime;

			if(shotDelayCounter <= 0)
			{
				shotDelayCounter = shotDelay;
				Instantiate (fireBall, firePoint.position, firePoint.rotation);
			}
		}
	}

	public void Jump()
	{
		GetComponent<Rigidbody2D>().velocity = new Vector2 (GetComponent<Rigidbody2D>().velocity.x, jumpHeight);
	}
}