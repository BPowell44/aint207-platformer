﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LifeManager : MonoBehaviour 
{
	//public int startingLives;
	private int lifeCounter;

	private Text theText;

	public GameObject gameOver;

	public PlayerController player;

	public string mainMenu;

	public float waitAfterGameOver;

	// Use this for initialization
	void Start () 
	{
		theText = GetComponent<Text> ();

		lifeCounter = PlayerPrefs.GetInt ("PlayerCurrentLives");

		player = FindObjectOfType<PlayerController> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		theText.text = lifeCounter + " x";

		if (lifeCounter == 0)
		{
			gameOver.SetActive(true);
			player.gameObject.SetActive (false);
		}

		if (gameOver.activeSelf) 
		{	
			waitAfterGameOver -= Time.deltaTime;
		}

		if (waitAfterGameOver < 0)
		{
			Application.LoadLevel (mainMenu);	
		}
	}

	public void MinusLife ()
	{
		lifeCounter--;
		PlayerPrefs.SetInt ("PlayerCurrentLives", lifeCounter);
	}
}
