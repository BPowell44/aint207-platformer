﻿using UnityEngine;
using System.Collections;

public class CoinPickup : MonoBehaviour 
{

	public int pointToAdd;

	public AudioSource coinPickUpSound;

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.GetComponent<PlayerController> () == null)
			return;

		ScoreManager.AddPoints (pointToAdd);
		coinPickUpSound.Play ();
		Destroy (gameObject);
	}
}
