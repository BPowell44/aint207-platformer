﻿using UnityEngine;
using System.Collections;

public class DestroyObjectOverTime : MonoBehaviour 
{
	// Creates a float called lifeTime
	public float lifeTime;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		// minus 1 of the variable every second
		lifeTime -= Time.deltaTime;

		// is lifeTime is less than 0 then
		if (lifeTime < 0) 
		{
			// Destroy this game object
			Destroy (gameObject);
		}
	
	}
}
