﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour 
{
	public int maxPlayerHealth;
	public static int PlayerHealth;
	

	private LevelManager levelManager;

	public float heartHealth;

	public bool isDead;

	private LifeManager lifesystem;

	public Sprite heartOne;
	public Sprite heartTwo;
	public Sprite heartThree;

	// Use this for initialization
	void Start () 
	{
		PlayerHealth = maxPlayerHealth;
		levelManager = FindObjectOfType<LevelManager> ();	
		lifesystem = FindObjectOfType<LifeManager> ();
		isDead = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (PlayerHealth <= 0 && !isDead) 
		{
			PlayerHealth = 0;
			levelManager.RespawnPlayer ();
			lifesystem.MinusLife ();
			isDead = true;
		}

		if (PlayerHealth == 30)
		{
			gameObject.GetComponent<Image> ().sprite = heartThree;
		}

		if (PlayerHealth == 20)
		{
			gameObject.GetComponent<Image> ().sprite = heartTwo;
		}

		if (PlayerHealth == 10)
		{
			gameObject.GetComponent<Image> ().sprite = heartOne;
		}
	}

	public static void HurtPlayer (int damageToGive)
	{
		PlayerHealth -= damageToGive;
	}

	public void FullHealth()
	{
		PlayerHealth = maxPlayerHealth;
	}
}
